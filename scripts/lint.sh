#!/usr/bin/env bash

# Undo any edits made by this script if no preexisting uncommitted changes.
cleanup() {
  git reset --hard HEAD
}
if [ $(git status --porcelain | wc -l) == "0" ]; then
    trap cleanup EXIT
fi

gofmtFiles=$(find . -name '*.go' | grep -v vendor | xargs gofmt -l)
if [ -n "${gofmtFiles}" ]; then
    echo 'gofmt needs running on the following files:'
    echo "${gofmtFiles}"
    echo "You can use the command: \`make fmt\` to reformat code."
    exit 1
fi

lintErrors=$(golint ./... | grep -v "should have comment or be unexported")
if [ $(echo -n "$lintErrors" | wc -l) -gt "0" ]; then
    echo "Linting errors:\n $lintErrors"
    exit 1
fi
go vet ./...

# Check that our modules are tidy and committed.
go mod tidy
git diff --exit-code -- go.mod go.sum || \
    (echo; echo "Unexpected difference in go.mod/go.sum files. Make sure your changes are committed. 
    Run 'go mod tidy' command or revert any go.mod/go.sum changes and commit."; 
    exit 1)
