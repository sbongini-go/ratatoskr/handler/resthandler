package resthandler

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"testing"

	"github.com/phayes/freeport"
	"github.com/rs/zerolog"
	"gitlab.alm.poste.it/go/ilog/ilog"
	izerolog "gitlab.alm.poste.it/go/ilog/zerolog"
	"gitlab.com/sbongini-go/ratatoskr/core"
	"gitlab.com/sbongini-go/ratatoskr/core/handler/customhandler"
	"gitlab.com/sbongini-go/ratatoskr/core/handler/echohandler"
	"gitlab.com/sbongini-go/ratatoskr/core/model"
	"gitlab.com/sbongini-go/ratatoskr/source/httpsource"
	"gitlab.com/sbongini-go/ratatoskr/source/httpsource/route"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/exporters/stdout/stdouttrace"
	"gotest.tools/assert"
)

func startCoreServerHTTP() (core.Core, int, error) {
	// Canale su cui arrivera una notifica quando il Core e' ready
	chanReady := make(chan struct{})

	// Trovo una porta casuale libera che impostero come porta del server HTTP
	serverPort, err := freeport.GetFreePort()
	if nil != err {
		return core.Core{}, 0, err
	}

	logger := izerolog.NewZeroLog(
		izerolog.WithLogger(zerolog.New(os.Stderr).With().Timestamp().Logger()),
		izerolog.WithLevel(ilog.DEBUG),
	)

	// Istanzio gli exporter per OpenTelemetry
	jaegerExporter, _ := jaeger.New(jaeger.WithCollectorEndpoint(jaeger.WithEndpoint("http://jaeger:14268/api/traces")))
	stdoutExporter, _ := stdouttrace.New()

	coreObj := core.NewCore(
		core.WithServiceName("ratatoskr"),
		core.WithServiceVersion("v0.1.0"),
		core.WithServiceNamespace("ns-demo"),
		core.WithServiceAttribute("environment", "demo"),
		core.WithLogger(logger),
		core.WithSpanExporter(stdoutExporter),
		core.WithSpanExporter(jaegerExporter),
		core.WithChanReady(chanReady),
		core.WithSource("http-source", httpsource.New(
			httpsource.WithPort(serverPort),
			httpsource.WithRoute(
				route.Builder().
					Method("GET").
					Path("/api/v1/test/0/:id").
					HeaderMapping("Content-Type", "Content-Type").
					Handler("rest-get").
					Build(),
			),
			httpsource.WithRoute(
				route.Builder().
					Method("GET").
					Path("/api/v1/test/1/:id").
					HeaderMapping("Content-Type", "Content-Type").
					Handler("response").
					Build(),
			),
			httpsource.WithRoute(
				route.Builder().
					Method("POST").
					Path("/api/v1/test/0/:id").
					HeaderMapping("Content-Type", "Content-Type").
					Handler("rest-post").
					Build(),
			),
			httpsource.WithRoute(
				route.Builder().
					Method("POST").
					Path("/api/v1/test/1/:id").
					HeaderMapping("Content-Type", "Content-Type").
					Handler("echo").
					Build(),
			),
		)),
		core.WithHandler("rest-get", New(fmt.Sprintf("%s%d%s", "http://localhost:", serverPort, "/api/v1/test/1/1234"))),
		core.WithHandler("rest-post", New(fmt.Sprintf("%s%d%s", "http://localhost:", serverPort, "/api/v1/test/1/1234"), WithMethod("POST"))),
		core.WithHandler("response", customhandler.New(func(ctx context.Context, bridge *core.Bridge, event model.Event) (model.Event, error) {
			return model.Event{
				Metadata: map[string][]string{
					"datacontenttype": []string{"application/json; charset=UTF-8"},
				},
				Data: []byte(`{"name":"amber"}`),
			}, nil
		})),
		core.WithHandler("echo", echohandler.New()),
	)

	go coreObj.Start()

	<-chanReady // Dopo questa riga il Core sara ready

	return coreObj, serverPort, nil
}

func TestHttpSource(t *testing.T) {
	coreObj, serverPort, err := startCoreServerHTTP()
	assert.NilError(t, err)
	defer coreObj.Stop()

	t.Run("get", func(t *testing.T) {
		request, err := http.NewRequest("GET", fmt.Sprintf("http://localhost:%d%s", serverPort, "/api/v1/test/0/1234"), bytes.NewBuffer([]byte{}))
		assert.NilError(t, err)
		request.Header.Set("Content-Type", "application/json; charset=UTF-8")

		httpClient := &http.Client{}
		resp, err := httpClient.Do(request)
		assert.NilError(t, err)

		bodyBytes, err := ioutil.ReadAll(resp.Body)
		assert.NilError(t, err)
		fmt.Println("HEADER:", resp.Header)
		fmt.Println("RESPONSE:", string(bodyBytes))
		assert.Equal(t, string(bodyBytes), `{"name":"amber"}`)
		assert.Equal(t, resp.Header.Get("Content-Type"), "application/json; charset=UTF-8")
	})

	t.Run("post", func(t *testing.T) {
		var jsonData = []byte(`{"name":"bluebell"}`)
		request, err := http.NewRequest("POST", fmt.Sprintf("http://localhost:%d%s", serverPort, "/api/v1/test/0/1234"), bytes.NewBuffer(jsonData))
		assert.NilError(t, err)
		request.Header.Set("Content-Type", "application/json; charset=UTF-8")

		httpClient := &http.Client{}
		resp, err := httpClient.Do(request)
		assert.NilError(t, err)

		bodyBytes, err := ioutil.ReadAll(resp.Body)
		assert.NilError(t, err)
		fmt.Println("HEADER:", resp.Header)
		fmt.Println("RESPONSE:", string(bodyBytes))
		assert.Equal(t, string(bodyBytes), `{"name":"bluebell"}`)
		assert.Equal(t, resp.Header.Get("Content-Type"), "application/json; charset=UTF-8")
	})
}
