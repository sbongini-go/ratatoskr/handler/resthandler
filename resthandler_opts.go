package resthandler

import "time"

// Option tipo funzione che rappresenta un opzione funzionale per inizializzare il restHandler
type Option func(*restHandler)

// WithMethod imposta il verbo HTTP da usare per effettuare la richiesta HTTP
func WithMethod(method string) Option {
	return func(h *restHandler) {
		h.method = method
	}
}

// WithTimeout imposta il timeout delle rchieste HTTP
func WithTimeout(timeout time.Duration) Option {
	return func(h *restHandler) {
		h.timeout = timeout
	}
}

// WithHeader aggiunge un header fisso alla richieste HTTP
func WithHeader(key string, value string) Option {
	return func(h *restHandler) {
		h.headers[key] = value
	}
}

// WithHeaders aggiunge degli headers fissi alle richieste HTTP
func WithHeaders(headers map[string]string) Option {
	return func(h *restHandler) {
		for key, value := range headers {
			h.headers[key] = value
		}
	}
}
