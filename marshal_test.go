package resthandler

import (
	"fmt"
	"testing"

	"gopkg.in/yaml.v2"
	"gotest.tools/assert"
)

func CommonTestUnmarshal(t *testing.T, in interface{}) {
	yamlData, err := yaml.Marshal(in)
	assert.NilError(t, err)
	fmt.Println(string(yamlData))

	// UnMarshal
	cfg := make(map[string]interface{})
	err = yaml.Unmarshal(yamlData, &cfg)
	assert.NilError(t, err)

	out, err := NewFromInterface(cfg)
	assert.NilError(t, err)
	fmt.Println(out)

	// Marshal
	yamlData2, err := yaml.Marshal(out)
	assert.NilError(t, err)
	fmt.Println(string(yamlData2))

	assert.Equal(t, string(yamlData), string(yamlData2))
}

func TestUnmarshal(t *testing.T) {
	CommonTestUnmarshal(t, New("http://localhost:9999/api/v1/test/1/1234"))
	CommonTestUnmarshal(t, New("http://localhost:9999/api/v1/test/1/1234", WithMethod("POST")))
}
