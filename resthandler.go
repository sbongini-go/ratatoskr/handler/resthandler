package resthandler

import (
	"context"
	"time"

	"gitlab.com/sbongini-go/mapstructureplus"
	"gitlab.com/sbongini-go/ratatoskr/core"
	"gitlab.com/sbongini-go/ratatoskr/core/handler"
	"gitlab.com/sbongini-go/ratatoskr/core/model"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"

	"github.com/go-resty/resty/v2"
)

func NewFromInterface(data interface{}) (core.Handler, error) {
	h := newDefault()
	err := mapstructureplus.PrivateDecode(data, &h)
	return &h, err
}

func (h restHandler) MarshalYAML() (interface{}, error) {
	return struct {
		Name    string
		Type    string
		URL     string
		Method  string
		Timeout time.Duration
		Headers map[string]string
	}{
		Name:    h.Name(),
		Type:    h.Type(),
		URL:     h.url,
		Method:  h.method,
		Timeout: h.timeout,
		Headers: h.headers,
	}, nil
}

type restHandler struct {
	// embedded type contente le parti comuni degli handlers
	handler.CommonHandler `mapstructure:",squash"`

	// url url da chiamare con la richiesta HTTP
	url string `yaml:"url"  mapstructure:"url"`

	// method metodo HTTP da usare per effettuare la request
	method string `yaml:"method"  mapstructure:"method"`

	timeout time.Duration     `yaml:"timeout"  mapstructure:"timeout"`
	headers map[string]string `yaml:"headers"  mapstructure:"headers"`

	// client rest
	client *resty.Client `yaml:"-"`
}

// newDefault instanzia un REST handler di default
func newDefault() restHandler {
	return restHandler{
		client:  resty.New(), // Creo il client REST
		method:  "GET",       // Imposto il verbo HTTP di default
		timeout: 30 * time.Second,
		headers: make(map[string]string),
	}
}

// RestHandler istanzia un nuovo RestHandler
func New(url string, opts ...Option) core.Handler {
	h := newDefault()
	h.url = url

	// Racchiudo l'HTTP Client Transport in quello di OpenTelemetry cosi da iniettare gli Span nelle richieste
	// questo transport rrichisce anche lo span con tutte le informazioni necessarie
	h.client.SetTransport(otelhttp.NewTransport(h.client.GetClient().Transport))

	// Applico gli option patterns
	for _, opt := range opts {
		opt(&h)
	}

	h.client.SetTimeout(h.timeout)
	h.client.SetHeaders(h.headers)

	return &h
}

// Execute esegue la logica del Handler
func (h restHandler) Execute(ctx context.Context, bridge *core.Bridge, event model.Event) (model.Event, error) {
	// Creo la request HTTP
	request := h.client.R().SetContext(ctx)

	// Imposto gli header della request
	request = request.SetHeaderMultiValues(event.Metadata)

	// Imposto/sovrascrivo il Content-Type della request usanto il DataContentType
	if contentType := event.DataContentType(); contentType != "" {
		request = request.SetHeader("Content-Type", contentType)
	}

	// Imposta il body della richiesta HTTP
	request = request.SetBody(event.Data)

	// Esegue la richiesta HTTP
	response, err := request.Execute(h.method, h.url)
	if nil != err {
		return model.Event{}, err
	}

	// Creo l'evento di output
	outputEvent := model.Event{
		Data:     response.Body(),   // Payload del messaggio
		Metadata: response.Header(), // Metadati associati al messaggio
	}

	if contentType := response.Header().Get("Content-Type"); contentType != "" {
		outputEvent.SetDataContentType(contentType)
	}

	return outputEvent, nil
}

// Type restituisce il tipo di handler
func (h *restHandler) Type() string {
	return "rest"
}
